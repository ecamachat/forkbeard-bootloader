# Makefile

TOOLCHAIN_DIR	?= $(abspath $(shell dirname $$(dirname $$(which arm-none-eabi-gcc))))
NRF_SDK_DIR     ?= /Users/eric/Projects/GitHub/nrfconnect/v1.9.1
SW_SIGNING_KEY	?= $(abspath keys/signing-dev.pem)
PYTHON_CMD	    ?= python3

BOARD            = nrf5340dk_nrf5340_cpuapp
TOOLCHAIN_VAR    = gnuarmemb
ZEPHYR_DIR       = $(NRF_SDK_DIR)/zephyr
SHELL_ENV		 = export BOARD=$(BOARD); \
				   export ZEPHYR_TOOLCHAIN_VARIANT=$(TOOLCHAIN_VAR); \
				   export GNUARMEMB_TOOLCHAIN_PATH=$(TOOLCHAIN_DIR); \
	               source $(ZEPHYR_DIR)/zephyr-env.sh;
CMAKE_CMD		 = $(SHELL_ENV) $(addprefix env ,$(CMAKE_ENV)) cmake
IMGTOOL			 = $(NRF_SDK_DIR)/bootloader/mcuboot/scripts/imgtool.py
ASSEMBLE		 = $(NRF_SDK_DIR)/bootloader/mcuboot/scripts/assemble.py
PYOCD			 = pyocd

BL_SLOT_SIZE	 = 0x10000
FW_SLOT_SIZE	 = 0xF0000
FW_HEADER_LEN	 = 0x200
FLASH_ALIGNMENT	 = 8

ifeq (,$(SW_VERSION))
	SW_VERSION		:= \"$(shell git describe --tags --match '[0-9]*.[0-9]*' | sed  -e 's,-,+,' -e 's,-g.*,,')\"
else
	CMAKE_ARGS	+= -DEXTRA_CFLAGS=-DSW_VERSION=\\\"$(SW_VERSION)\\\"
endif

# MCU boot configs
CMAKE_ARGS      += -DBOARD=$(BOARD)
CMAKE_ARGS      += -DCONFIG_BOOT_SIGNATURE_KEY_FILE=\"$(SW_SIGNING_KEY)\"
CMAKE_ARGS		+= -DCONFIG_FPROTECT=n
#CMAKE_ARGS		+= -DCONFIG_MCUBOOT_IMAGE_VERSION=$(SW_VERSION)
#CMAKE_ARGS		+= -DCONFIG_BOOT_IMAGE_ACCESS_HOOKS=y
#CMAKE_ARGS		+= -DCONFIG_BOOT_IMAGE_ACCESS_HOOKS_FILE=\"$(abspath mcuboot/hooks/movano_hooks.c)\"


CMAKE_GEN = 'Unix Makefiles'
BUILD_CMD = $(MAKE)
ifeq (1,$(V))
	CMAKE_ARGS  += -DCMAKE_VERBOSE_MAKEFILE=ON
	CMAKE_ARGS  += -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
else
	CMAKE_ARGS  += -DCMAKE_VERBOSE_MAKEFILE=OFF
    # Prefer Samurai/Ninja then Unix Makefiles
	ifneq (0,$(PREFER_NINJA))
		ifneq (,$(shell which samu))
			CMAKE_GEN = 'Ninja'
			BUILD_CMD = $(shell which samu)
		else
			ifneq (,$(shell which ninja))
				CMAKE_GEN = 'Ninja'
				BUILD_CMD = $(shell which ninja)
			endif
		endif
	endif
endif

.SUFFIXES:            # Delete the default suffixes

all: build

config: build/.configured

build: build/.built $(shell find . -type f -name '*.c' -o -type f -name '*.h')

reconfig:
	$(MAKE) clean config

rebuild:
	$(MAKE) clean build

build/.created:
	@[ -d build ] || mkdir build
	@touch build/.created

build/.configured: build/.build-tools Makefile $(wildcard conf/*) $(wildcard keys/*)
	$(addprefix env ,$(CMAKE_ENV)) $(CMAKE_CMD) -DOVERLAY_CONFIG=$(abspath conf/overlay-movano.conf) -G $(CMAKE_GEN) -S $(NRF_SDK_DIR)/bootloader/mcuboot/boot/zephyr -B build $(CMAKE_ARGS)
	@touch build/.configured

build/.built: build/.build-tools build/.configured
	$(addprefix env ,$(BUILD_ENV)) $(BUILD_CMD) -C build all $(addprefix VERBOSE=,$(V))
	@touch build/.built

clean:
	@[ -d build ] && rm -rf build || true

show-memory-map:
	@$(addprefix env ,$(BUILD_ENV)) $(BUILD_CMD) -C build partition_manager_report
	@#if [ -n "$(wildcard build/partitions*.yml)" ]; then $(PYTHON_CMD) $(NRF_SDK_DIR)/nrf/scripts/partition_manager_report.py --input $(wildcard build/partitions*.yml); else echo "No partition conf file."; false; fi

reset:
	nrfjprog -f NRF53 --reset

flash: flash-boot

flash-erase:
	nrfjprog -f NRF53 --recover

flash-boot: build/.flash-tools build/.built
	nrfjprog -f NRF53 --verify --reset --program build/zephyr/zephyr.hex --sectorerase

check-build-tools: build/.build-tools

build/.build-tools: build/.created
	@if [ $$(sh -c 'cmake --help >/dev/null 2>/dev/null; echo $$?') -eq 127 ]; then echo "cmake not found!"; false; else true; fi
	@if [ $$(sh -c '$(BUILD_CMD) --help >/dev/null 2>/dev/null; echo $$?') -eq 127 ]; then echo "$(BUILD_CMD) not found!"; false; else true; fi
	@touch build/.build-tools

check-flash-tools: build/.flash-tools
	@if [ $$(sh -c 'nrfjprog --help >/dev/null 2>/dev/null; echo $$?') -eq 127 ]; then echo "nrfjprog not found!"; false; else true; fi

build/.flash-tools: build/.created
	@if [ $$(sh -c 'nrfjprog --help >/dev/null 2>/dev/null; echo $$?') -eq 127 ]; then echo "nrfjprog not found!"; false; else true; fi
	@touch build/.flash-tools

gen-signing-keys:
	@if ! [ -f $(SW_SIGNING_KEY) ]; then openssl ecparam -name prime256v1 -genkey -noout -out $(SW_SIGNING_KEY); fi

menuconfig:
	$(addprefix env ,$(BUILD_ENV)) $(BUILD_CMD) -C build menuconfig

help:
	@echo "make <clean|config|build|rebuild|flash-boot|flash-erase|show-memory-map|gen-signing-keys|menuconfig>"

print-%:
	@echo $* = $($*)

.PHONY: all build rebuild check-build-tools check-flash-tools clean config reconfig flash flash-erase flash-app gen-signing-keys menuconfig help reset show-memory-map
